class SessionsController < Devise::SessionsController#create
  def new
    super
  end

  def create
    if Tokens.find_all_by_c_id(current_courier_profile.id).length == 0

      @token = (0...128).map { ('a'..'z').to_a[rand(26)] }.join
      line = Tokens.new()
      line.c_id = current_courier_profile.id
      line.token = @token
      line.save

    else
      @token = Tokens.find_by_c_id(current_courier_profile.id).token
    end
    render  json: { token: @token }
  end

  def update
    super
  end
end
