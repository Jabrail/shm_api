# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
APIShopm::Application.config.secret_key_base = '30f598ee6573a9bbe1fc76c45e42cb86bd650bf811d786171b7c522f496ee33e6ce480b1cc16ceabc840ffe324e0081add7ed9cfd6a66ea69dc98f7461c46b50'
