class CourierApiController < ApplicationController

  def authentification

    if CourierProfile.find_by_email(params[:email])

if Devise.secure_compare(params[:pass], CourierProfile.find_by_email(params[:email]).encrypted_password)
puts '2'
        if Tokens.find_all_by_c_id(CourierProfile.find_by_email(params[:email]).id).length == 0

          @token = (0...128).map { ('a'..'z').to_a[rand(26)] }.join
          line = Tokens.new()
          line.c_id = current_courier_profile.id
          line.token = @token
          line.save

        else
          @token = Tokens.find_by_c_id(CourierProfile.find_by_email(params[:email]).id).token
        end
        render  json: { token: @token }

      end

    end
    render  json: { token: @token }
  end

  def auth_true

    if Tokens.find_all_by_c_id(current_courier_profile.id).length == 0

      @token = (0...128).map { ('a'..'z').to_a[rand(26)] }.join
      line = Tokens.new()
      line.c_id = current_courier_profile.id
      line.token = @token
      line.save

    else
      @token = Tokens.find_by_c_id(current_courier_profile.id).token
    end
    render  json: { token: @token }
  end

  def get_orders

    if (Tokens.find_by_token(params[:token]))
      c_id = Tokens.find_by_token(params[:token])
      token = (0...128).map { ('a'..'z').to_a[rand(26)] }.join
      Tokens.find_by_token(params[:token]).update(:token => token)

      way_bill = WaybillsName.find_all_by_courier_id(CouriersList.find_by_login(CourierProfile.find(c_id).email).id)

      order_list = Array.new

      way_bill.each do |way|

        if way.status == 2

          relation = WayTdRelations.find_all_by_waybill_id(way.id)
          puts relation.length
          markets = Array.new
          all_markets = Array.new
          all_orders = Array.new

          relation.each do |rel|
            all_markets << Order.find(rel.order_id).c_a_r_id
            all_orders << Order.find(rel.order_id)
          end

          all_markets = all_markets.uniq

          all_markets.each do |market|

            items = Array.new

            all_orders.each do |order|

              if order.c_a_r_id == market

                item = Item.find(order.item_id)
                items << { "name" => item.name , "count" => order.count , "order_id" => order.id}
              end
            end

            mark = CardMarket.find_by_m_id(market)
            markets << {"name" => Market.find(market).name, "country" => mark.country , "city" => mark.city , "address" => mark.address , "contact_name" => mark.contact_name, "phone" => mark.phone , "lon" => mark.lon , "lat" => mark.lat , "items" => items }

          end

          order_list << { "orders" => markets}

        end

      end
      render  json: { token: token , market: order_list}
    end
  end

  def set_status
  end

  def set_las_location

    if (Tokens.find_by_token(params[:token]))
      c_id = Tokens.find_by_token(params[:token])
      token = (0...128).map { ('a'..'z').to_a[rand(26)] }.join
      Tokens.find_by_token(params[:token]).update(:token => token)

      loc = CourierLocation.new
      loc.c_id = c_id
      loc.lat = params[:lat]
      loc.lon = params[:lon]

      if loc.save
        render  json: { token: token , status: 'complete'}
      else
        render  json: { token: token , status: 'error'}
      end
    end

  end
end
