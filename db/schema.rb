# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140228161034) do

  create_table "autos", force: true do |t|
    t.integer  "u_id"
    t.string   "number"
    t.string   "type_auto"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "td_id"
  end

  create_table "c_a_relations", force: true do |t|
    t.integer  "u_id"
    t.integer  "c_id"
    t.integer  "a_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "td_id"
  end

  create_table "card_markets", force: true do |t|
    t.string   "country"
    t.string   "city"
    t.string   "address"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "contact_name"
    t.integer  "m_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
    t.string   "lon"
    t.string   "lat"
  end

  create_table "card_trading_platforms", force: true do |t|
    t.string   "country"
    t.string   "city"
    t.string   "line"
    t.string   "rating"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "tp_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.integer  "m_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parentCat"
  end

  create_table "courier_locations", force: true do |t|
    t.integer  "c_id"
    t.string   "lon"
    t.string   "lat"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courier_profiles", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "courier_profiles", ["email"], name: "index_courier_profiles_on_email", unique: true, using: :btree
  add_index "courier_profiles", ["reset_password_token"], name: "index_courier_profiles_on_reset_password_token", unique: true, using: :btree

  create_table "couriers", force: true do |t|
    t.integer  "u_id"
    t.string   "name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  create_table "couriers_lists", force: true do |t|
    t.string   "login"
    t.string   "pass"
    t.string   "name"
    t.string   "last_name"
    t.integer  "td_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: true do |t|
    t.string   "name"
    t.integer  "price"
    t.string   "currency"
    t.integer  "count"
    t.integer  "code_number"
    t.integer  "id_td"
    t.integer  "category_id"
    t.string   "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "marketplaces", force: true do |t|
    t.integer  "u_id"
    t.string   "name"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "markets", force: true do |t|
    t.string   "name"
    t.integer  "u_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "my_couriers", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_couriers", ["email"], name: "index_my_couriers_on_email", unique: true, using: :btree
  add_index "my_couriers", ["reset_password_token"], name: "index_my_couriers_on_reset_password_token", unique: true, using: :btree

  create_table "orders", force: true do |t|
    t.integer  "mp_id"
    t.integer  "c_a_r_id"
    t.integer  "item_id"
    t.integer  "count"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags_relations", force: true do |t|
    t.integer  "tp_id"
    t.integer  "tag_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "td_couriers", force: true do |t|
    t.string   "login"
    t.string   "pass"
    t.string   "name"
    t.string   "last_name"
    t.integer  "td_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tokens", force: true do |t|
    t.string   "token"
    t.integer  "c_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tp_and_market_relations", force: true do |t|
    t.integer  "m_id"
    t.integer  "tp_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "types", force: true do |t|
    t.string   "name"
    t.string   "val"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "way_td_relations", force: true do |t|
    t.integer  "td_id"
    t.string   "waybill_id"
    t.string   "integer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  create_table "waybills", force: true do |t|
    t.integer  "order_id"
    t.integer  "m_id"
    t.integer  "courier_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status"
  end

  create_table "waybills_names", force: true do |t|
    t.string   "name"
    t.integer  "td_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "auto_id"
    t.integer  "courier_id"
    t.integer  "status"
    t.string   "order_date"
  end

end
