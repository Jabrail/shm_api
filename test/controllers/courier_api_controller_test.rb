require 'test_helper'

class CourierApiControllerTest < ActionController::TestCase
  test "should get auth_true" do
    get :auth_true
    assert_response :success
  end

  test "should get get_orders" do
    get :get_orders
    assert_response :success
  end

  test "should get set_status" do
    get :set_status
    assert_response :success
  end

  test "should get set_las_location" do
    get :set_las_location
    assert_response :success
  end

end
