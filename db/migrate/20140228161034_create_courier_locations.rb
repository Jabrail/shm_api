class CreateCourierLocations < ActiveRecord::Migration
  def change
    create_table :courier_locations do |t|
      t.integer :c_id
      t.string :lon
      t.string :lat

      t.timestamps
    end
  end
end
